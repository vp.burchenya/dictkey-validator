"""Module for JSON-compatible dictionary key validators"""


__all__ = ('MongoKeyValidator', 'ValidationError')


from typing import NamedTuple
from typing import List, Union, Iterator, Callable, Any


class ValidationError(NamedTuple):
    """Holds failed validation result description in `message`
    attribute and place where failure is happen in `path` attribute"""
    path: tuple
    message: str


class KeyValidator:  # pylint: disable=too-few-public-methods
    """Responsible for validating keys in a JSON-compatible dictionary

    Example::

       validator = KeyValidator(
           lambda k: len(k) or 'Empty keys are not allowed',
           lambda k: '.' not in k or 'Key cannot contain "."'
       )
       for err in validator.validate({'...': 'admin'}):
           print(f'{err.path}: {err.message}')
    """

    def __init__(self, *predicates: Callable[[Any], Union[bool, str]]):
        """Predicate Functions:
           Signature:
               predicate_func(key: Any) -> Union[bool, str]
                 + key: key to validate
                 + returns: True if key is valid; error string otherwise

        :param predicates: predicate functions
        """
        self._predicates = predicates

    def validate(self, doc: dict) -> List[ValidationError]:
        """Main method for doing a validation itself

        :param doc: document whose keys need to validate
        :return: list of `ValidationError`s or empty list if all keys are valid
        """
        return self.__visit(doc)

    def __visit(self, doc: Any, *path: str) -> List[ValidationError]:
        """Just a helper for hiding a recursive nature of `validate` method"""

        errors: List[ValidationError] = []

        if isinstance(doc, dict):
            for k, val in doc.items():
                current_path = (*path, k)
                errors.extend(
                    ValidationError(current_path, err_msg,)
                    for err_msg
                    in self.__get_errors(k)
                )
                errors.extend(self.__visit(val, *current_path))

        elif isinstance(doc, (set, list, tuple)):
            for k, val in enumerate(doc):
                errors.extend(self.__visit(val, *path, str(k)))

        return errors

    def __get_errors(self, value: str) -> Iterator[str]:
        """Execute predicates on a key value"""
        for predicate in self._predicates:
            err = predicate(value)
            if isinstance(err, str):
                yield err
            elif not err:
                yield 'Key is a not valid'


# pylint: disable=too-few-public-methods
class MongoKeyValidator(KeyValidator):
    """
    MongoDB document key validator

    Validate a document keys with considering restrictions of MongoDB key name
    rules, see: https://docs.mongodb.com/manual/core/document/#field-names

    Example::

       mongo_kv = MongoKeyValidator()
       errors = mongo_kv.validate({ '$user.login': 'admin' })
    """

    def __init__(self) -> None:
        super().__init__(
            lambda k: isinstance(k, str),
            lambda k: k != '_id',
            lambda k: not k.startswith('$') or "Key name started with '$'",
            lambda k: '.' not in k or "Key with '.' in name",
        )
