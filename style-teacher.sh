 #!/bin/bash

for pred in 'mypy'\
            'autopep8 --diff --exit-code'\
            'pylint'\
            'flake8'
do
  CMD="python3 -m ""$pred"
  echo "Validate with: ""$CMD ""$*"
  if eval "$CMD" "$*" 2>&1 1>/dev/null ; then
    continue
  else
    echo "$pred failed"
    exit 255
  fi
done
